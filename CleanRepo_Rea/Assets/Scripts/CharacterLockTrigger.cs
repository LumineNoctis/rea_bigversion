using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLockTrigger : MonoBehaviour
{
    public GameObject deactiveObj;
    public GameObject activeObj;
    public GameObject activeCamera;
    public bool reapersCutscene;


    public void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            CharacterMovement.LockCharacter();
            if (deactiveObj != null)
                deactiveObj.SetActive(false);

            if (activeCamera != null)
                activeCamera.SetActive(true);

            if (activeObj != null)
                activeObj.SetActive(true);

            if (reapersCutscene)
                NPCNonLoopingWaypoint.walkSignal = 2; 
        }
    }

    public void UnlockCharacter()
    {
        CharacterMovement.UnlockCharacter();
    }

}
