using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GoToReapersHQ : MonoBehaviour
{
    public TMP_Text textPanelUI;
    public string interactionMessage;
    public bool spokeToTess;
    public bool spokeToNiira;
    public Animator transitionAnim;
    
    
    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player")
            return;

        if (textPanelUI != null)
            textPanelUI.text = interactionMessage;
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {

            if (Input.GetKeyDown(KeyCode.E))
            {
                SceneManager.LoadScene("RepearsHeadquarters");
            }
                
                
        }

        if (other.tag == "Player")
            if (spokeToTess == true)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    SceneManager.LoadScene("SecondTimeRedmartScene");
                }
                    
            } 
        
        
        if (other.tag == "Player")
            if (spokeToNiira == true)
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    SceneManager.LoadScene("FinalScene");
                }
            }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag != "Player")
            return;

        if (textPanelUI == null)
            return;

        ClearUIText();
    }

    public void ClearUIText()
    {
        textPanelUI.text = " ";
    }
}
