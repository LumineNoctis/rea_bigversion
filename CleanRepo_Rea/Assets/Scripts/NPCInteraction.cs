using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCInteraction : MonoBehaviour
{
    public GameObject cape;
    public GameObject noCape;

    public GameObject capeMesh;
    public GameObject noCapeMesh;


    public void SwitchBeatreceDialog()
    {
        capeMesh.SetActive(false);
        noCapeMesh.SetActive(false);

        if (ProgressionManager.isCapeOn)
            cape.SetActive(true);
        else
            noCape.SetActive(true);
    }

    public void SwitchBeatreceBack()
    {
        cape.SetActive(false);
        noCape.SetActive(false);

        if (ProgressionManager.isCapeOn)
            capeMesh.SetActive(true);
        else
            noCapeMesh.SetActive(true);
    }
}
