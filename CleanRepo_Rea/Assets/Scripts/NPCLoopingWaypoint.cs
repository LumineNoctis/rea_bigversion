using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCLoopingWaypoint : MonoBehaviour
{
    public Animator NPCAnimator;

    public bool walking;
    public float speed;
    bool rotate;
    float timer;
    public float waitTime;
    public Waypoints wpoints;
    private int waypointIndex;
    public bool talking;

    void Start()
    {
        rotate = true;
    }


    void Update()
    {
        MoveNPC();
    }

    public void MoveNPC()
    {
        if (!walking)
        {
            NPCAnimator.Play("Idling");

            if (talking)
               return;

            timer += Time.deltaTime;
            if (timer>= waitTime)
            {
                timer = 0;
                walking = true;
            }
            return;
        }

        if (rotate)
        {
            transform.LookAt(wpoints.waypoints[waypointIndex]);
            rotate = false;
        }

        NPCAnimator.Play("Walking");

        if (!talking)
        { 

            transform.position = Vector3.MoveTowards(transform.position, wpoints.waypoints[waypointIndex].position, speed * Time.deltaTime);

            if ((transform.position - wpoints.waypoints[waypointIndex].position).sqrMagnitude < 5f)
            {
                walking = false;
                rotate = true;
                waypointIndex++;
                if (waypointIndex == wpoints.waypoints.Length)
                {
                    waypointIndex = 0; 

                    return;
                }
            }
        }
    }

    public void StartInteraction()
    {
        talking = true;
        walking = false;
    }
    public void StopInteraction()
    {
        talking = false;
        walking = true;
    }
}
