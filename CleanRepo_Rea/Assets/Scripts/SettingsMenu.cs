/*using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public AudioSource audioSource;
    public TMP_Dropdown qualityDropdownTMP;
    public TMP_Dropdown resolutionDropdownTMP;

    Resolution[] resolutions;

    private float soundVolume = 1f;

    void Start ()
    {
        return;

        audioSource.Play();

        resolutions = Screen.resolutions;

        resolutionDropdownTMP.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && 
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdownTMP.AddOptions(options);
        resolutionDropdownTMP.value = currentResolutionIndex;
        resolutionDropdownTMP.RefreshShownValue();
    }

    void Update()
    {
        return;
        //audioSource.volume = soundVolume;
    }

    public void SetResolution(int resolutionIndex)
    {
        //resolutionIndex = resolutionDropdownTMP.value;
        //Resolution resolution = resolutions[resolutionIndex];
        //Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetVolume (float volume)
    {
        //soundVolume = volume;
    }

    public void SetQuality ()
    {
        //QualitySettings.SetQualityLevel(qualityDropdownTMP.value);
        //print($"Quality settings is now set on : {QualitySettings.GetQualityLevel().ToString()} : {QualitySettings.names[QualitySettings.GetQualityLevel()]}");
    }

    public void SetFullScreen (bool isFullscreen)
    {
        //Screen.fullScreen = isFullscreen;
    }
}*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public AudioSource audioSource;

    public Dropdown resolutionDropdown;

    Resolution[] resolutions;

    private float soundVolume = 1f;

    void Start ()
    {
        audioSource.Play();

        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && 
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }

    void Update()
    {
        audioSource.volume = soundVolume;
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetVolume (float volume)
    {
        soundVolume = volume;
    }

    public void SetQuality (int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullScreen (bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }
}