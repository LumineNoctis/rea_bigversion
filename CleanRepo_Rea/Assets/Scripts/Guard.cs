using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guard : MonoBehaviour
{
    public static event System.Action OnGuardHasSpottedPlayer;
    public Light spotlight;
    public float viewDistance;
    public float timeToSpotPlayer = .5f;
    public LayerMask viewMask;
    float viewAngle;
    float playerVisibleTimer;
    public GameObject dialogueTrigger;

    Transform player;
    Color originalSpotlightColor;

    // Start is called before the first frame update
    void Start()
    {
        viewAngle = spotlight.spotAngle;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        originalSpotlightColor = spotlight.color;
        dialogueTrigger.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (CanSeePlayer ())
        {
            playerVisibleTimer += Time.deltaTime;
        }
        else 
        {
            playerVisibleTimer -= Time.deltaTime;
        }
        playerVisibleTimer = Mathf.Clamp (playerVisibleTimer, 0, timeToSpotPlayer);
        spotlight.color = Color.Lerp(originalSpotlightColor,Color.red, playerVisibleTimer / timeToSpotPlayer);

        if (playerVisibleTimer >= timeToSpotPlayer)
        {
            if(OnGuardHasSpottedPlayer != null)
            {
                OnGuardHasSpottedPlayer();
                dialogueTrigger.SetActive(true);
            }
        }
    }

    bool CanSeePlayer()
    {
        if(Vector3.Distance(transform.position,player.position) < viewDistance)
        {
            Vector3 dirToPlayer = (player.position - transform.position).normalized;
            float angleBetweenGuardAndPlayer = Vector3.Angle (transform.forward, dirToPlayer);

            if(angleBetweenGuardAndPlayer < viewAngle / 2f)
            {
                if(!Physics.Linecast(transform.position,player.position,viewMask))
                {
                    return true;
                }
            }
        }
        return false;
    }
}
