using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCNonLoopingWaypoint : MonoBehaviour
{
    [SerializeField] private GameObject character;
    public Animator charAnimator;

    private Transform startPos;
    public Transform endingPos;
    public bool walking;
    public float speed;
    bool rotate;
    public Rigidbody rb;
    public static int walkSignal;

    public Waypoints wpoints;
    private int waypointIndex;

    void Start()
    {
        startPos = character.transform;
        rotate = true;
    }


    void Update()
    {
        Move();
        if (walkSignal > 0)
        {
            walking = true;
            walkSignal--; 
        }
    }

    public void Move()
    {
        if (!walking)
        {
            rb.angularVelocity = Vector3.zero;
            return;
        }

        if (wpoints.waypoints[waypointIndex] == null)
            walking = false;

        if (rotate)
        {
            transform.LookAt(wpoints.waypoints[waypointIndex]);
            rotate = false;
        }

        charAnimator.Play("Walking");
        transform.position = Vector3.MoveTowards(transform.position, wpoints.waypoints[waypointIndex].position, speed * Time.deltaTime);

        if ((transform.position - wpoints.waypoints[waypointIndex].position).sqrMagnitude < 5f)
        {
            rotate = true;
            waypointIndex++;
            if (waypointIndex == wpoints.waypoints.Length)
            {
                walking = false;
                charAnimator.Play("Idling");
                return;
            }
        }

    }

    private void OnDestroy()
    {
        walkSignal = 0; 
    }
}
