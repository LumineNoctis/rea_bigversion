using UnityEngine;
using System.Linq;

public class PickUp : Interactable
{       
    public HealthBar healthBar;
    public UITrigger uITrigger;
    public int iD;
    public static bool checkedList;

    public override void Interact()
    {
        healthBar.AddHP(1);
        uITrigger.ClearUIText();
        ProgressionManager.flowersStates[iD] = true;
        gameObject.SetActive(false);
    }

    private void Start()
    {
        uITrigger = GetComponent<UITrigger>();

        if (!checkedList && !ProgressionManager.listInitialized)
        {
            checkedList = true;
            ProgressionManager.listInitialized = true;
        }


        if (checkedList)
        {
            ProgressionManager.flowersStates.Add(iD, false);
        }
        else
        {
            gameObject.SetActive(!ProgressionManager.flowersStates[iD]);
        }
    }

    private void OnDestroy()
    {
        checkedList = false; 
    }

}

