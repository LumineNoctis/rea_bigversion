using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject spawnPoint;
    public GameObject player;
    public float respawnDelay;
    bool spotted;
    public GameObject guards1;
    public GameObject guards2;
    
    void Start()
    {
        Guard.OnGuardHasSpottedPlayer += SpawnPlayer;
    }
    void OnDestroy()
    {
        Guard.OnGuardHasSpottedPlayer -= SpawnPlayer;
        
        if (guards1 != null)
            guards1.SetActive(true);
        
        if (guards2 != null)
            guards2.SetActive(true);
    }

    private void SpawnPlayer()
    {
        if(spotted)
        {
            return;
        }
        spotted = true;
        Invoke("PlayerToSpawn", respawnDelay);
    }

    void PlayerToSpawn()
    {
        spotted = false;
        player.transform.position = spawnPoint.transform.position;
        DialogManager.Instance.EndDialog();
    }

    public void Cheat()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            guards1.SetActive(false);
            guards2.SetActive(false);
        }
    }
}
