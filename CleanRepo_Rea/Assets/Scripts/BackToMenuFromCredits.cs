using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class BackToMenuFromCredits : MonoBehaviour
{
    bool creditsStarted;
    [SerializeField] Animator anim;
    [SerializeField] Image panel;

    float timer;
    bool panelBlack;

    private void OnEnable()
    {
        creditsStarted = true;
    }

    private void Update()
    {
        if (!panelBlack)
        {
            timer += Time.deltaTime;

            if (timer >= 10)
            {
                panel.color = Color.black;
                panelBlack = true;
            }
        }

        if (creditsStarted)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Done"))
                SceneManager.LoadScene("Menu");
        }
    }
}
