using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour 
{
    public virtual void Interact() { }
    public bool disabled;
    public bool requiredKeyInput;
    public string interactMessage;

}
