using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;

    public void AddHP(int hpGain)
    {
        slider.value += hpGain;
    }        
}
