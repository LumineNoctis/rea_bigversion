using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
   

    public Animator DoorAnimator;

    public void TriggerDoor(){
        DoorAnimator.SetTrigger("TriggerDoor");
    }

}
