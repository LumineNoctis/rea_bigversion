using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

// on dialog manager object (just an empty object with this script)
public class DialogManager : MonoBehaviour
{
	public static DialogManager Instance;
	public CharacterMovement characterMoveGO;
	public static CharacterMovement characterMove;
	static DialogLine currentDialogLine;
	public DialogLine firstDialogLine;
	public TMP_Text textBoxGOLocked;
	public TMP_Text textBoxGOUnlocked;
	public static TMP_Text textBox;
	static float timer;
	static bool countdown;
	public Animator camAnimator;
	public NiiraAnimation niira;
	public GameObject _textPanelUILocked;
	public GameObject _textPanelUIUnlocked;
	public static GameObject textPanelUI;
	public bool noDialogAtStart; 
	
	
	

	void OnEnable()
    {
		if (Instance == null)
			Instance = this; 
    }


	void Start()
    {
		characterMove = characterMoveGO;
		textBoxGOLocked.text = "";
		textBoxGOUnlocked.text = "";
		GetActiveTextBox(true);


		if (!noDialogAtStart)
		{
			CharacterMovement.locked = true;
			TriggerDialog(true, firstDialogLine, 0f);
		}

		//here! locking cursor works, but it kind of 'overrides' the unlocking of TriggerDialog() (dialogmanager)
		if (noDialogAtStart)
			Cursor.lockState = CursorLockMode.Locked;
	}

	void Update()
    {
		if (timer > 0)
			countdown = true;

		else if (countdown)
		{
			countdown = false;
			EndDialog();
			
		}

		if (countdown) 
			timer -= Time.deltaTime;
    }

	public static void GetActiveTextBox(bool locked)
    {
		textPanelUI = locked ? Instance._textPanelUILocked : Instance._textPanelUIUnlocked;
		textBox = locked ? Instance.textBoxGOLocked : Instance.textBoxGOUnlocked;
    }
	
	public void OptionIndexSignal(int _index)
	{
		if (!textPanelUI.activeSelf)
        {
			textPanelUI.SetActive(true);
        }

		foreach (DialogLine line in DialogLine.dialogLines)
		{
			if (line.index == _index)
            {
				line.ActivateChildren(true);
				textBox.text = line.line;
				currentDialogLine.ActivateChildren(false);
				currentDialogLine = line;
            }
		}
	}
	//DialogManager.DialogTrigger(line to trigger(LineX))
	public static void TriggerDialog(bool lockCharacter, DialogLine lineToTrigger, float _timer)
    {
		textPanelUI.SetActive(true);
		Cursor.lockState = lockCharacter ? CursorLockMode.None : CursorLockMode.Locked;
		Cursor.visible = true; 
		if (lockCharacter)
		{
			CharacterMovement.active = false;
		}
		currentDialogLine = lineToTrigger;
		textBox.text = lineToTrigger.line;
		lineToTrigger.ActivateChildren(true);
		timer = _timer;

	}

	public void EndDialog()
    {
		//camAnimator.enabled = false;
		if(niira!= null)
		{niira.walking = true;}
		
		CharacterMovement.locked = false; 
		CharacterMovement.active = true;
		currentDialogLine.ActivateChildren(false);
		Cursor.lockState = CursorLockMode.Locked;
		textBox.text = "";
		textPanelUI.SetActive(false);
	}

	private void OnApplicationQuit()
    {
		Instance = null; 
    }

	private void OnDisable()
    {
		Instance = null; 
    }
}
