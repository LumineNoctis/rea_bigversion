using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GoBackToDesert : MonoBehaviour
{
    public GameObject niiraCamera;
    public GameObject desertNiira;


    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            
            niiraCamera.SetActive(true);
            desertNiira.SetActive(true);
        }
    }

    public void LoadDesert()
    {
        Debug.Log("Desert");
        SceneManager.LoadScene("DesertScene");

    }

}
