using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GameStates {PAUSE, PLAY, SETTINGS};

public class PauseGame : MonoBehaviour
{
    public static GameStates gameState;
    public GameObject pausePanel;
    public GameObject settingsPanel;
    CursorLockMode _cursorLockMode;
    public GameObject creditsPanel;


    void Start()
    {
        gameState = GameStates.PLAY;
        pausePanel.SetActive(false);
        settingsPanel.SetActive(false);
    }

    void Update()
    {
        if(gameState == GameStates.PLAY || gameState == GameStates.PAUSE)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && gameState != GameStates.PAUSE)
                PausePlay();
            else if (Input.GetKeyDown(KeyCode.Escape) && gameState == GameStates.PAUSE)
                ResumeGame();
        }

    }

    public void PausePlay()
    {
        _cursorLockMode = Cursor.lockState;
        gameState = GameStates.PAUSE;
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        if (creditsPanel != null)
            creditsPanel.SetActive(false);

        Cursor.lockState = CursorLockMode.None;
        CharacterMovement.LockCharacter();

    }

    public void ResumeGame()
    {
        gameState = GameStates.PLAY;
        Time.timeScale = 1;
        pausePanel.SetActive(false);
        Cursor.lockState = _cursorLockMode;
        CharacterMovement.UnlockCharacter();


    }
}
