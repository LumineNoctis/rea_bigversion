using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NiiraAnimation : MonoBehaviour
{
    [SerializeField] private GameObject niira;
    public Animator niiraAnimator;

    private Transform startPos;
    public Transform endingPos;
    public bool walking;
    public float speed;
    bool rotate;
    public Rigidbody rb;

    public Waypoints wpoints;
    private int waypointIndex;

    void Start()
    {
        startPos = niira.transform;
        rotate = true;
    }


    void Update()
    {
        MoveNiira();
    }

    public void MoveNiira()
    {
        if (!walking)
        {
            rb.angularVelocity = Vector3.zero;
            return;
        }
        
        if (rotate)
        {
            transform.LookAt(wpoints.waypoints[waypointIndex]);
            rotate = false;
        }

        niiraAnimator.Play("Walking");
        transform.position = Vector3.MoveTowards(transform.position, wpoints.waypoints[waypointIndex].position, speed * Time.deltaTime);

        if ((transform.position - wpoints.waypoints[waypointIndex].position).sqrMagnitude < 5f)
        {
            rotate = true;
            waypointIndex++;
            if (waypointIndex == wpoints.waypoints.Length)
            {
                walking = false;
                niiraAnimator.Play("Idling");
                return;
            }
        }

    }

}
