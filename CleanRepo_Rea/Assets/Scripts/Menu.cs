using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{

    //public Image fader;



    //IEnumerator LoadDesertCorout(bool fadeAway)
    //{
    //    // fade from opaque to transparent
    //    if (fadeAway)
    //    {
    //        // loop over 1 second backwards
    //        for (float i = 1; i >= 0; i -= Time.deltaTime)
    //        {
    //            // set color with i as alpha
    //            fader.color = new Color(0, 0, 0, i);
    //            yield return null;
    //        }
    //    }
    //    // fade from transparent to opaque
    //    else
    //    {
    //        fader.color = Color.clear;
    //        // loop over 1 second
    //        for (float i = 0; i <= 1; i += Time.deltaTime)
    //        {
    //            // set color with i as alpha
    //            fader.color = new Color(0, 0, 0, i);
    //            yield return null;

    //        }
    //        yield return new WaitForSeconds(1);
    //        Debug.Log("Desert");
    //        SceneManager.LoadScene("DesertScene");
    //    }
    //}

    //public void LoadDesert()
    //{
    //    StartCoroutine(LoadDesertCorout(false));
    //}

    public void LoadRedMart()
    {
        //Fade();
        Debug.Log("RedMart");
        SceneManager.LoadScene("RedmartScene");
    }
    
    public void LoadDesert()
    {
        //Fade();
        Debug.Log("Desert");
        SceneManager.LoadScene("DesertScene");
    }

    public void LoadSecondTimeInRedmart()
    {
        //Fade();
        SceneManager.LoadScene("SecondTimeRedmartScene");
    }

    public void BackToMenu()
    {
        //Fade();
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("Menu");
    }

    public void QuitGame()
    {
        //Fade();
        Debug.Log("Quit");
        Application.Quit();
    }

    //public void Fade()
    //{
    //    transitionAnim.SetTrigger("FadeOut"); 
    //}

    //private void OnDestroy()
    //{
    //    transitionAnim.ResetTrigger("FadeOut")
    //}

}