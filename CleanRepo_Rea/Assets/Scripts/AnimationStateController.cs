using UnityEngine;

public class AnimationStateController : MonoBehaviour
{
    [HideInInspector] public Animator animator;
    public GameObject noCapeBeatrece;
    public GameObject capeBeatrece;
    public Animator noCapeAnim;
    public Animator capeAnim;

    public GameObject noCapeSword;
    public GameObject capeSword;

    bool walking;
    bool running;

    private void Start()
    {
        if (ProgressionManager.isCapeOn)
        {
            animator = capeAnim;
            capeBeatrece.SetActive(true);
            noCapeBeatrece.SetActive(false);
        }
        else
        {
            animator = noCapeAnim;
            noCapeBeatrece.SetActive(true);
            capeBeatrece.SetActive(false);
        }
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.J))
        //    SwitchBeatrece();

        if (CharacterMovement.locked || !CharacterMovement.active)
        {
            animator.SetBool("isWalking", false);
            animator.SetBool("isRunning", false);

            walking = false;
            running = false;
            return;
        }

        if (!walking && Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || 
        Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D))
        {
            animator.SetBool("isWalking", true);
            walking = true;
        }
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.A) || 
        Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D))
        {
            animator.SetBool("isWalking", false);
            walking = false;
        }

        if (!running && Input.GetKey(KeyCode.LeftShift))
        {
            animator.SetBool("isRunning", true);
            running = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            animator.SetBool("isRunning", false);
            running = false;
        }
    }

    public void SwitchBeatrece()
    {
        ProgressionManager.isCapeOn = !ProgressionManager.isCapeOn;

        if (ProgressionManager.isCapeOn)
        {
            animator = capeAnim;
            capeBeatrece.SetActive(true);
            noCapeBeatrece.SetActive(false);
        }
        else
        {
            animator = noCapeAnim;
            noCapeBeatrece.SetActive(true);
            capeBeatrece.SetActive(false);
        }
    }

    public void GetSword()
    {
        if (ProgressionManager.isCapeOn)
            capeSword.SetActive(true);
        else
            noCapeSword.SetActive(true);
    }

    public void LeaveSword()
    {
        if (ProgressionManager.isCapeOn)
            capeSword.SetActive(false);
        else
            noCapeSword.SetActive(false);
    }

}
