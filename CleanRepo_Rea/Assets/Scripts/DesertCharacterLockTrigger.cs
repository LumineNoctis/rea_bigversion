using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesertCharacterLockTrigger : MonoBehaviour
{
    public GameObject cameraRedMart;
    public GameObject redMartNiira;
    public GameObject walkingNiira;
    public GameObject desertNiira;
    public GameObject cameraDesertNiira;


    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            CharacterMovement.LockCharacter();
            if (walkingNiira != null)
                walkingNiira.SetActive(false);


            if (cameraRedMart != null)
                cameraRedMart.SetActive(true);


            if (redMartNiira != null)
                redMartNiira.SetActive(true);
        }
    }

    public void UnlockCharacter()
    {
        CharacterMovement.UnlockCharacter();
    }
}