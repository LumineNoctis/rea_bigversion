using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogLine : MonoBehaviour
{
	public string line;
	public int index;

	static bool initialized;

	public static List<DialogLine> dialogLines;

	void Start()
	{
		if (!initialized)
		{
			dialogLines = new List<DialogLine>();
			initialized = true;
		}

		dialogLines.Add(this);
	}

	public void ActivateChildren(bool activate)
    {
		List<Transform> children = new List<Transform>();

		foreach (Transform child in transform)
        {
			children.Add(child);
        }

		foreach (Transform child in children)
        {
			child.gameObject.SetActive(activate);
        }
    }

	private void OnDisable()
    {
		dialogLines = new List<DialogLine>();
		initialized = false; 
    }
}
