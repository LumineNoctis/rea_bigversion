using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterMovement : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;
    float defaultSpeed;
    public float runSpeed;

    public Camera cam;

    float bodyRotation;
    float headTiltRotation;
    public float lookSpeed;
    public static bool active;
    public static bool locked;
    public Animator animNoCape;
    public AudioSource audioS;
    AnimationStateController aSC;
    Transform meshNoCape;
    Transform meshCape;
    public float rotationAnimThreshold;

    public float collDetectionRadius;
    public LayerMask layerMask;

    private void Awake()
    {
        if (SceneManager.GetActiveScene().name == "RedmartScene")
            ProgressionManager.beenInRedmart = true;

        aSC = GetComponent<AnimationStateController>();

        meshNoCape = aSC.noCapeBeatrece.transform;
        meshCape = aSC.capeBeatrece.transform;
    }

    private void Start()
    {
        defaultSpeed = speed;
        if (!ProgressionManager.beenInRedmart && animNoCape.enabled)
            animNoCape.SetTrigger("getUp");

        if (!locked)
            active = true; 
    }

    void Update()
    {
        if (locked || !active )
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            return;
        }

        Move();
        MouseLook();
    }

    bool RaycastCollision(Vector3 moveDirection)
    {

        if(Physics.Raycast(transform.position + Vector3.up * 3.5f, moveDirection, collDetectionRadius, layerMask))
        {
            speed = 0;
            return false;
        }

        else if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = runSpeed;
        }

        else
        {
            speed = defaultSpeed;
        }
        return true;
    }

    void Move()
    {
        Vector3 direction = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            direction += transform.forward;
        if (Input.GetKey(KeyCode.A))
            direction -= transform.right;
        if (Input.GetKey(KeyCode.S))
            direction -= transform.forward;
        if (Input.GetKey(KeyCode.D))
            direction += transform.right;

        if (direction == Vector3.zero)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            return;
        }
        else
            direction = Vector3.Normalize(direction);

        if(RaycastCollision(direction))
        {
            rb.MovePosition(transform.position + direction * speed * Time.deltaTime);
        }
    }

    void MouseLook()
    {
        bodyRotation = Input.GetAxis("Mouse X") * lookSpeed;

        if (Mathf.Abs(bodyRotation) >= rotationAnimThreshold)
            aSC.animator.SetBool("isRotating", true);
        else
            aSC.animator.SetBool("isRotating", false);

        headTiltRotation = cam.transform.eulerAngles.x - 
            Input.GetAxis("Mouse Y") * lookSpeed;

        if (headTiltRotation < 310 && headTiltRotation > 55)
            headTiltRotation = headTiltRotation > 180 ? 310 : 55;

        transform.eulerAngles += new Vector3(0, bodyRotation, 0);
        cam.transform.eulerAngles = 
            new Vector3( 
            headTiltRotation, 
            cam.transform.eulerAngles.y, 
            cam.transform.eulerAngles.z);
       
        bodyRotation = 0;
        headTiltRotation = 0;
    }


    public static void LockCharacter()
    {
        locked = true;
    }

    public static void UnlockCharacter()
    {
        locked = false; 
    }

   void OnDestroy()
    {
        locked = false;
        active = false; 
    }
}
