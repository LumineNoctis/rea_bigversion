using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.EventSystems;
using TMPro;

public class UITrigger : MonoBehaviour
{
    Interactable interactable;
    public TMP_Text textPanelUI;
    //public float fadeTime;
        
    void Start()
    {
        interactable = GetComponent<Interactable>();
        //_textPanelUI = GameObject.Find("Text").GetComponent<Text>();
        //_textPanelUI.color = Color.clear;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player")
            return;

        if (textPanelUI != null)
            textPanelUI.text = interactable.interactMessage; 
    }

    void OnTriggerStay(Collider other)
    {

        if (other.tag == "Player" && !interactable.disabled)
        {
            if (interactable.requiredKeyInput && !Input.GetKeyDown(KeyCode.E))
                return;

            if (GetComponent<NPCLoopingWaypoint>() != null)
            {
                NPCLoopingWaypoint nPCLoopingWaypoint = GetComponent<NPCLoopingWaypoint>();
                nPCLoopingWaypoint.StartInteraction();
            }

            interactable.Interact();

            if (textPanelUI != null)
                ClearUIText();
        }
    }

   void OnTriggerExit(Collider other)
    {
        if (other.tag != "Player")
            return;

        if (textPanelUI == null)
            return; 

        ClearUIText();
        DialogManager.Instance.EndDialog();
        if (GetComponent<NPCLoopingWaypoint>() != null)
        {
            NPCLoopingWaypoint nPCLoopingWaypoint = GetComponent<NPCLoopingWaypoint>();
            nPCLoopingWaypoint.StopInteraction();
        }
    }

    public void ClearUIText()
    {
        textPanelUI.text = " ";
    }

    /*void FadeText()
    {
        if(displayText)
        {
            myText.text = myString;
            myText.color = Color.Lerp(myText.color, Color.white, fadeTime * fadeTime.deltaTime);
        }

        else 
        {
            myText.color = Color.Lerp(myText.color, Color.clear, fadeTime * fadeTime.deltaTime);
        }
    }*/

}
