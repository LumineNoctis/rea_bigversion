using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : Interactable
{
    public DialogLine lineToTrigger;
    public bool lockCharacter;
    public float timer;
    public GameObject activeCamera;
    public bool repeatable; 

    public override void Interact()
    {
        DialogManager.GetActiveTextBox(lockCharacter);
        DialogManager.TriggerDialog(lockCharacter, lineToTrigger, timer);
        if (!repeatable)
            disabled = true;

        if (activeCamera != null)
            activeCamera.SetActive(true);
    }
}
